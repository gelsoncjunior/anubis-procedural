const req = require("request")
const cfg = require("../config/cfg")
const log = require("./util")

function buscarDocument(document, tj) {
  let headers = {
    "Authorization": `Bearer ${cfg.access_token}`,
    "X-Requested-With": "XMLHttpRequest",
    "Accept": "application/json",
    "Content-Type": "application/json",
  }

  let body = {
    "numero_documento": document,
    "send_callback": false,
    "wait": 1
  }

  let options = {
    url: cfg.url.process_cpf.replace('-TJ-', tj),
    headers,
    body: JSON.stringify(body)
  }

  req.post(options, (err, res) => {
    console.log("Status:" + res.statusCode)
    if (err) {
      console.log(err)
      log('err', err)
    } else {
      log('info', JSON.stringify(res.body))
      var options = {
        headers: { "content-type": "application/x-www-form-urlencoded" },
        url: "http://192.168.88.122:8080/ords/granihc/v1/register/proc_document/",
        form: {
          api_data: `[${JSON.stringify(JSON.parse(res.body))}]`
        }
      };
      req.post(options, (err, res) => {
        log('info', JSON.stringify(res.body))
      })
      log('info', JSON.stringify(res.body))
    }
  })

}

module.exports = buscarDocument