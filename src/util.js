const fs = require("fs")
const datetime = require("node-datetime")

const skyLog = "logs/anubis-DATE.out"

function currentDateTimeSystem() {
  let dt = datetime.create()
  let dtime = dt.format('d-m-Y H:M:S')
  let date = dt.format('d-m-Y')
  return {
    dtime, date
  }
}

function logLevel(level) {
  switch (level) {
    case "info":
      levelReturn = '[INFO]'
      break
    case "warn":
      levelReturn = '[WARN]'
      break
    case "err":
      levelReturn = '[ERROR]'
      break
    case "crit":
      levelReturn = '[CRITICAL]'
      break
    default:
      levelReturn = '[UNK]'
  }
  return levelReturn
}

function log(level, msg) {

  let lvl = logLevel(level)
  let fmtDate = currentDateTimeSystem()
  let msgCompleted = `${lvl} - ${fmtDate.dtime} > ${msg}`
  let pathFile = skyLog.replace('-DATE', `-${fmtDate.date}`)

  fs.appendFile(pathFile, `\n${msgCompleted}`, err => {
    if (err) {
      console.log(`> Failed write log`)
    }
  })
}

module.exports = log