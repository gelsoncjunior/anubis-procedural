const express = require("express")
const bodyParser = require("body-parser")

const app = express();

//body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(3333, () => {
  console.log(`> Running On: 3333`);
});

module.exports = app;
