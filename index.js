const req = require("request")
const app = require("./config/server");
const rdocument = require("./src/busca_document")
const rnome = require("./src/busca_nome")
const rtermo = require("./src/busca")
const log = require("./src/util")

app.get("/nome/:name/tj/:tj", (req, res) => {
  let nome = req.params.name
  let tj = req.params.tj.toUpperCase()
  log('info', `Nome: ${nome}, ${tj}`)
  rnome(nome, tj)
  res.send({ msg: "processing" })
})

app.get("/document/:document/tj/:tj", (req, res) => {
  let document = req.params.document
  let tj = req.params.tj.toUpperCase()
  log('info', `Nome: ${document}, ${tj}`)
  rdocument(document, tj)
  res.send({ msg: "processing" })
})

app.get("/termo/:termo", (req, res) => {
  let termo = req.params.termo
  log('info', `Nome: ${termo}`)
  rtermo(termo)
  res.send({ msg: "processing" })
})

